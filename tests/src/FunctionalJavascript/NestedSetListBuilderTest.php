<?php

namespace Drupal\Tests\nested_set\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the nested set list builder.
 */
class NestedSetListBuilderTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['nested_set_test'];

  /**
   * The storage of the test entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function setUp() {
    parent::setUp();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->storage = $entity_type_manager->getStorage('entity_nested_set_test');

    $account = $this->drupalCreateUser(['administer entity_nested_set_test']);
    $this->drupalLogin($account);
  }

  /**
   * Tests modifying the entity hierarchy using the list builder.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testHierarchy() {
    $page = $this->getSession()->getPage();
    $session = $this->assertSession();

    $count = 5;
    $ids = [];
    $names = [];
    for ($i = 0; $i < $count; ++$i) {
      $this->drupalGet('/entity_nested_set_test/add');
      $names[$i] = $page->findField('Name')->getValue();
      $page->pressButton('Save');

      preg_match('#/entity_nested_set_test/(?<id>\d+)/edit$#', $this->getUrl(), $matches);
      $ids[$i] = $matches['id'];
    }

    $this->assertPositions($ids, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    $this->drupalGet('/entity_nested_set_test');
    $table = $page->find('css', 'table#edit-entities');
    $session->elementsCount('css', 'thead tr', 1, $table);
    $session->elementsCount('css', 'thead tr th', 3, $table);
    $header = $page->findAll('css', 'thead tr th');
    /* @var \Behat\Mink\Element\NodeElement $name_header */
    /* @var \Behat\Mink\Element\NodeElement $operations_header */
    list($name_header, /* $weight_header */, $operations_header) = $header;
    $this->assertEquals('Name', $name_header->getText());
    $this->assertEquals('Operations', $operations_header->getText());
    $session->elementsCount('css', 'tbody tr.draggable', $count);
    $rows = $page->findAll('css', 'tbody tr.draggable');
    /** @var \Behat\Mink\Element\NodeElement[] $handles */
    $handles = [];
    /** @var \Behat\Mink\Element\NodeElement[] $labels */
    $labels = [];
    foreach ($names as $i => $name) {
      /* @var \Behat\Mink\Element\NodeElement $row */
      $row = $rows[$i];
      $session->elementsCount('css', 'td', 3, $row);
      $name_cell = $row->find('css', 'td');
      // The tabledrag handle adds a space to front of the name.
      $this->assertEquals(' ' . $name, $name_cell->getText());
      $session->elementExists('css', 'a.tabledrag-handle', $name_cell);

      $handles[$i] = $name_cell->find('css', 'a.tabledrag-handle');
      $labels[$i] = $name_cell->find('css', 'span.entity-nested-set-test-label');
    }

    $handles[0]->dragTo($handles[1]);
    $page->pressButton('Save');
    $this->assertPositions($ids, [3, 4, 1, 2, 5, 6, 7, 8, 9, 10]);

    $handles[1]->dragTo($labels[1]);
    $page->pressButton('Save');
    $this->assertPositions($ids, [2, 3, 1, 4, 5, 6, 7, 8, 9, 10]);

    $handles[2]->dragTo($labels[2]);
    $handles[2]->dragTo($labels[2]->getParent()->findAll('css', 'div.js-indentation')[1]);
    $handles[3]->dragTo($labels[3]);
    $handles[3]->dragTo($labels[3]->getParent()->findAll('css', 'div.js-indentation')[1]);
    $handles[2]->dragTo($labels[2]);
    $handles[4]->dragTo($labels[4]);
    $handles[4]->dragTo($labels[4]);
    $page->pressButton('Save');
    $this->assertPositions($ids, [2, 5, 1, 10, 3, 4, 6, 9, 7, 8]);
  }

  /**
   * Asserts that the test entities have the correct position values.
   *
   * @param int[]|string[] $ids
   *   The IDs of the test entities.
   * @param int[] $positions
   *   The expected positions of the test entities.
   */
  public function assertPositions($ids, $positions) {
    $this->storage->resetCache($ids);
    /* @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
    $entities = $this->storage->loadMultiple($ids);
    $this->assertCount(count($ids), $entities);
    foreach ($entities as $entity) {
      $this->assertEquals(array_shift($positions), $entity->get('hierarchy')->lft);
      $this->assertEquals(array_shift($positions), $entity->get('hierarchy')->rgt);
    }
    $this->assertEmpty($positions);
  }

}
