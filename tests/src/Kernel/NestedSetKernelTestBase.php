<?php

namespace Drupal\Tests\nested_set\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Provides a base class for nested set kernel tests.
 */
class NestedSetKernelTestBase extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['nested_set'];

  /**
   * The ID of the entity type used in the test.
   *
   * @var string
   */
  protected $entityTypeId = 'entity_test';

  /**
   * The field name of the nested set field that is being tested.
   *
   * @var string
   */
  protected $fieldName = 'nested_set_test';

  /**
   * The entity storage used in the test.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The nested set used in the test.
   *
   * @var \Drupal\nested_set\NestedSet\NestedSetExaminer
   */
  protected $nestedSet;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function setUp() {
    parent::setUp();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->storage = $entity_type_manager->getStorage($this->entityTypeId);

    $field_storage = $entity_type_manager->getStorage('field_storage_config')->create([
      'entity_type' => $this->entityTypeId,
      'field_name' => $this->fieldName,
      'type' => 'nested_set',
      'translatable' => FALSE,
    ]);
    $field_storage->save();
    $entity_type_manager->getStorage('field_config')->create([
      'field_storage' => $field_storage,
      'bundle' => $this->entityTypeId,
      'translatable' => FALSE,
      'required' => TRUE,
    ])->save();
  }

  /**
   * Creates an entity.
   *
   * @param array $field_values
   *   An array of field values for the nested set field.
   * @param string $langcode
   *   The language code to create the entity in.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The created entity.
   */
  protected function createEntity(array $field_values = [], $langcode = 'en') {
    $values = [
      'langcode' => $langcode,
    ];
    // @todo This check should not be necessary, ideally.
    if ($field_values) {
      $values = [
        $this->fieldName => $field_values,
      ];
    }
    return $this->storage->create($values);
  }

  /**
   * Asserts that an entity is at the expected position in the nested set.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to make the assertion for.
   * @param int $left
   *   The expected left position.
   * @param int $right
   *   The expected right position.
   */
  protected function assertPosition(ContentEntityInterface $entity, $left, $right) {
    $items = $entity->get($this->fieldName);
    $this->assertFalse($items->isEmpty());
    $this->assertCount(1, $items);

    $this->assertSame($left, $items->lft);
    $this->assertSame($right, $items->rgt);
  }

}
