<?php

namespace Drupal\Tests\nested_set\Kernel;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Tests the 'nested_set' field type.
 */
class NestedSetItemTest extends NestedSetKernelTestBase {

  /**
   * Tests organizing entities in a nested set.
   *
   * At the end of the test the nested set model has the following structure:
   * - a (1,2)
   * - b (3,4)
   * - c (5,8)
   *   - c_a (6,7)
   * - d (9,14)
   *   - d_a (10,11)
   *   - d_b (12,13)
   * - e (15,20)
   *   - e_a (16,17)
   *   - e_b (18,19)
   * - f (21,28)
   *   - f_a (22,23)
   *   - f_b (24,25)
   *   - f_c (26,27)
   * - g (29,34)
   *   - g_a (30,33)
   *     - g_a_a (31,32)
   */
  public function testHierarchy() {
    // Create the first element.
    $a = $this->createEntity();
    $this->entityValidateAndSave($a);
    $this->assertPosition($a, 1, 2);

    // The second element should be placed right after the first.
    $b = $this->createEntity();
    $this->entityValidateAndSave($b);
    $this->assertPosition($b, 3, 4);

    // Test that specifying left and right position explicitly works, as well.
    $c = $this->createEntity([
      'lft' => 5,
      'rgt' => 8,
    ]);
    $this->entityValidateAndSave($c);
    $this->assertPosition($c, 5, 8);
    // Test that specifying the parent will compute the position accordingly.
    $c_a = $this->createEntity([
      'prt' => $c,
    ]);
    $this->entityValidateAndSave($c_a);
    $this->assertPosition($c_a, 6, 7);

    // Test that multiple children get placed correctly, as well.
    $d = $this->createEntity([
      'lft' => 9,
      'rgt' => 14,
    ]);
    $this->entityValidateAndSave($d);
    $this->assertPosition($d, 9, 14);
    $d_a = $this->createEntity([
      'prt' => $d,
    ]);
    $this->entityValidateAndSave($d_a);
    $this->assertPosition($d_a, 10, 11);
    $d_b = $this->createEntity([
      'prt' => $d,
    ]);
    $this->entityValidateAndSave($d_b);
    $this->assertPosition($d_b, 12, 13);

    // Tests that a free interval is found before a pre-existing child.
    $e = $this->createEntity([
      'lft' => 15,
      'rgt' => 20,
    ]);
    $this->entityValidateAndSave($e);
    $this->assertPosition($e, 15, 20);
    $e_b = $this->createEntity([
      'lft' => 18,
      'rgt' => 19,
    ]);
    $this->entityValidateAndSave($e_b);
    $this->assertPosition($e_b, 18, 19);
    $e_a = $this->createEntity([
      'prt' => $e,
    ]);
    $this->entityValidateAndSave($e_a);
    $this->assertPosition($e_a, 16, 17);

    // Tests that a free interval is found in between pre-existing children.
    $f = $this->createEntity([
      'lft' => 21,
      'rgt' => 28,
    ]);
    $this->entityValidateAndSave($f);
    $this->assertPosition($f, 21, 28);
    $f_a = $this->createEntity([
      'lft' => 22,
      'rgt' => 23,
    ]);
    $this->entityValidateAndSave($f_a);
    $this->assertPosition($f_a, 22, 23);
    $f_c = $this->createEntity([
      'lft' => 26,
      'rgt' => 27,
    ]);
    $this->entityValidateAndSave($f_c);
    $this->assertPosition($f_c, 26, 27);
    $f_b = $this->createEntity([
      'prt' => $f,
    ]);
    $this->entityValidateAndSave($f_b);
    $this->assertPosition($f_b, 24, 25);

    // Test that elements can be nested.
    $g = $this->createEntity([
      'lft' => 29,
      'rgt' => 34,
    ]);
    $this->entityValidateAndSave($g);
    $this->assertPosition($g, 29, 34);
    $g_a = $this->createEntity([
      'lft' => 30,
      'rgt' => 33,
    ]);
    $this->entityValidateAndSave($g_a);
    $this->assertPosition($g_a, 30, 33);
    $g_a_a = $this->createEntity([
      'prt' => $g_a,
    ]);
    $this->entityValidateAndSave($g_a_a);
    $this->assertPosition($g_a_a, 31, 32);
  }

  /**
   * Tests that attempting to save an entity into a filled parent fails.
   */
  public function testNoFreeInterval() {
    $a = $this->createEntity();
    $this->entityValidateAndSave($a);
    $this->assertPosition($a, 1, 2);

    // Test that validation fails.
    $a_a = $this->createEntity([
      'prt' => $a,
    ]);
    $violations = $a_a->validate();
    $this->assertCount(1, $violations);
    $this->assertSame('The nested set model for <em class="placeholder">nested_set_test</em> does not have a free interval within <em class="placeholder">1</em> and <em class="placeholder">2</em>', (string) $violations->get(0)->getMessage());

    $this->expectException(EntityStorageException::class);
    $this->expectExceptionMessage("The nested set model for entity type '$this->entityTypeId' and field '$this->fieldName' does not have a free interval within 1 and 2");
    $a_a->save();
  }

}
