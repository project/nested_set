<?php

namespace Drupal\Tests\nested_set\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the 'nested_set' field type with a translatable field.
 */
class NestedSetItemTranslationTest extends NestedSetKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['language'];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function setUp() {
    parent::setUp();

    ConfigurableLanguage::createFromLangcode('af')->save();

    FieldStorageConfig::loadByName($this->entityTypeId, $this->fieldName)
      ->setTranslatable(TRUE)
      ->save();
    FieldConfig::loadByName($this->entityTypeId, $this->entityTypeId, $this->fieldName)
      ->setTranslatable(TRUE)
      ->save();
  }

  /**
   * Tests a translatable nested set field.
   *
   * At the end of the test there are two nested set models with the following
   * structure, respectively:
   * - en:
   *   - a (1,2)
   *   - b (3,4)
   *   - d (5,6)
   * - af:
   *   - a (1,2)
   *   - c (3,4)
   *   - d (5,8)
   *     - d_a (6,7)
   */
  public function testTranslation() {
    // Test that different translations of the same entity can have the same
    // nested set position.
    $a_en = $this->createEntity();
    $this->entityValidateAndSave($a_en);
    $this->assertPosition($a_en, 1, 2);
    $a_af = $a_en->addTranslation('af');
    $this->entityValidateAndSave($a_af);
    $this->assertPosition($a_af, 1, 2);

    // Test that different entities can have the same nested set position.
    $b_en = $this->createEntity();
    $this->entityValidateAndSave($b_en);
    $this->assertPosition($b_en, 3, 4);
    $c_af = $this->createEntity([], 'af');
    $this->entityValidateAndSave($c_af);
    $this->assertPosition($c_af, 3, 4);

    // Test that different translations of the same entity can have different
    // nested set positions.
    $d_en = $this->createEntity();
    $this->entityValidateAndSave($d_en);
    $this->assertPosition($d_en, 5, 6);
    $d_af = $d_en->addTranslation('af', [
      $this->fieldName => [
        'lft' => 5,
        'rgt' => 8,
      ],
    ]);
    $this->entityValidateAndSave($d_af);
    $this->assertPosition($d_af, 5, 8);

    // Test that setting the parent explicitly works with translated entities.
    $d_a_af = $this->createEntity([
      'prt' => $d_af,
    ]);
    $this->entityValidateAndSave($d_a_af);
    $this->assertPosition($d_a_af, 6, 7);
  }

}
