<?php

namespace Drupal\Tests\nested_set\Kernel;

use Drupal\field\Entity\FieldConfig;

/**
 * Tests the 'nested_set' field type.
 */
class NestedSetItemOptionalTest extends NestedSetKernelTestBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();

    // Make the nested set field non-required.
    FieldConfig::loadByName($this->entityTypeId, $this->entityTypeId, $this->fieldName)
      ->setRequired(FALSE)
      ->save();
  }

  /**
   * Tests a non-required nested set field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testOptionalField() {
    // Test that the nested set field does get populated automatically.
    /* @var \Drupal\Core\Entity\ContentEntityInterface $a */
    $a = $this->createEntity();
    $this->entityValidateAndSave($a);
    $this->assertTrue($a->get($this->fieldName)->isEmpty());

    $b = $this->createEntity([
      'lft' => 1,
      'rgt' => 2,
    ]);
    $this->entityValidateAndSave($b);
    $this->assertPosition($b, 1, 2);

    /* @var \Drupal\Core\Entity\ContentEntityInterface $c */
    $c = $this->createEntity();
    $this->entityValidateAndSave($c);
    $this->assertTrue($c->get($this->fieldName)->isEmpty());

    // Test that populating the nested set via the parent value works for
    // non-required fields, as well.
    $d = $this->createEntity([
      'lft' => 3,
      'rgt' => 6,
    ]);
    $this->entityValidateAndSave($d);
    $this->assertPosition($d, 3, 6);
    $d_a = $this->createEntity([
      'prt' => $d,
    ]);
    $this->entityValidateAndSave($d_a);
    $this->assertPosition($d_a, 4, 5);
  }

}
