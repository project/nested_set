<?php

namespace Drupal\nested_set_test\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\nested_set\Entity\NestedSetListBuilder;

/**
 * Provides a list builder for tested entities with a hierarchy.
 */
class EntityNestedSetTestListBuilder extends NestedSetListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $nestedSetFieldName = 'hierarchy';

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = [
      '#markup' => $entity->label(),
      // Add a class around the label so that we can easily fetch it in tests.
      /* @see \Drupal\Tests\nested_set\FunctionalJavascript\NestedSetListBuilderTest */
      '#prefix' => '<span class="entity-nested-set-test-label">',
      '#suffix' => '</span>',
    ];
    return $row + parent::buildRow($entity);
  }

}
