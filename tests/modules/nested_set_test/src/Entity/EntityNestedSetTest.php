<?php

namespace Drupal\nested_set_test\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_test\Entity\EntityTest;

/**
 * Defines the nested set test entity class.
 *
 * @ContentEntityType(
 *   id = "entity_nested_set_test",
 *   label = @Translation("Test entity with hierarchy"),
 *   label_collection = @Translation("Test entities with hierarchy"),
 *   label_plural = @Translation("test entities with hierarchy"),
 *   handlers = {
 *     "list_builder" = "Drupal\nested_set_test\Entity\EntityNestedSetTestListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_test\EntityTestForm",
 *       "edit" = "Drupal\entity_test\EntityTestForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "entity_nested_set_test",
 *   admin_permission = "administer entity_nested_set_test",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "collection" = "/entity_nested_set_test",
 *     "add-form" = "/entity_nested_set_test/add",
 *     "edit-form" = "/entity_nested_set_test/{entity_nested_set_test}/edit",
 *   },
 * )
 */
class EntityNestedSetTest extends EntityTest {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['hierarchy'] = BaseFieldDefinition::create('nested_set')
      ->setLabel(new TranslatableMarkup('Hierarchy'))
      ->setRequired(TRUE);

    return $fields;
  }

}
