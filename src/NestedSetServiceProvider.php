<?php

namespace Drupal\nested_set;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\nested_set\Migrate\MigrationSubscriber;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Provides services for the Nested Set module.
 */
class NestedSetServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['migrate'])) {
      $container->register('nested_set.migrate_subscriber', MigrationSubscriber::class)
        ->addArgument(new Reference('state'))
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('entity_field.manager'))
        ->addTag('event_subscriber');
    }
  }

}
