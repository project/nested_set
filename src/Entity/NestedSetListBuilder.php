<?php

namespace Drupal\nested_set\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\nested_set\NestedSet\NestedSetExaminerFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list builder for entities using a nested set field.
 *
 * To use this list builder, create a custom list builder for your entity type
 * that extends this one and additionally:
 * - Specify the field name of the nested set field with the $nestedSetFieldName
 *   property.
 * - Add a 'label' column by returning a corresponding key in
 *   EntityListBuilderInterface::buildHeader() and
 *   EntityListBuilderInterface::buildRow().
 *
 * Use TranslatableNestedSetListBuilder instead of this if the nested set field
 * is translatable.
 *
 * @see \Drupal\nested_set\Entity\TranslatableNestedSetListBuilder
 * @see \Drupal\Core\Config\Entity\DraggableListBuilder
 * @see \Drupal\taxonomy\Form\OverviewTerms
 */
abstract class NestedSetListBuilder extends EntityListBuilder implements FormInterface {

  /**
   * The field name of the nested set field.
   *
   * @var string
   */
  protected $nestedSetFieldName;

  /**
   * The key to use for the form element containing the entities.
   *
   * @var string
   */
  protected $entitiesKey = 'entities';

  /**
   * The nested set structure.
   *
   * @var array
   */
  protected $nestedSet = [];

  /**
   * The entities being listed.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected $entities = [];

  /**
   * Name of the entity's weight field or FALSE if no field is provided.
   *
   * @var string|bool
   */
  protected $weightKey = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The nested set examiner.
   *
   * @var \Drupal\nested_set\NestedSet\NestedSetExaminer
   */
  protected $nestedSetExaminer;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a nested set list builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\nested_set\NestedSet\NestedSetExaminerFactory $nested_set_examiner_factory
   *   The nested set examiner factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, FormBuilderInterface $form_builder, RendererInterface $renderer, NestedSetExaminerFactory $nested_set_examiner_factory) {
    parent::__construct($entity_type, $storage);

    $this->formBuilder = $form_builder;
    $this->renderer = $renderer;

    if (isset($this->nestedSetFieldName)) {
      $this->initializeNestedSetExaminer($nested_set_examiner_factory);
    }
  }

  /**
   * Initializes the nested set examiner
   *
   * @param \Drupal\nested_set\NestedSet\NestedSetExaminerFactory $nested_set_examiner_factory
   *   The nested set examiner factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function initializeNestedSetExaminer(NestedSetExaminerFactory $nested_set_examiner_factory) {
    $this->nestedSetExaminer = $nested_set_examiner_factory->get($this->entityTypeId, $this->nestedSetFieldName);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('form_builder'),
      $container->get('renderer'),
      $container->get('nested_set.examiner_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->entityTypeId . '_list';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->nestedSetFieldName . '.lft')
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    if (isset($this->nestedSetExaminer)) {
      $header['weight'] = t('Weight');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    if (isset($this->nestedSetExaminer)) {
      $weight = $this->nestedSetExaminer->getWeight($entity);
      // Override default values to markup elements.
      $row['#attributes']['class'][] = 'draggable';
      $row['#weight'] = $weight;

      // Add weight column.
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight for @title', ['@title' => $entity->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => ['class' => ['weight']],
      ];
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (isset($this->nestedSetExaminer)) {
      return $this->formBuilder->getForm($this);
    }
    return parent::render();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#tabledrag' => [],
    ];
    if (isset($form[$this->entitiesKey]['#header']['label'])) {
      $form[$this->entitiesKey]['#tabledrag'][] = [
        'action' => 'match',
        'relationship' => 'parent',
        'group' => 'parent',
        'subgroup' => 'parent',
        'source' => 'id',
        'hidden' => FALSE,
      ];
      $form[$this->entitiesKey]['#tabledrag'][] = [
        'action' => 'depth',
        'relationship' => 'group',
        'group' => 'depth',
        'hidden' => FALSE,
      ];
    }
    $form[$this->entitiesKey]['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'weight',
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (isset($this->nestedSetExaminer)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      if (isset($row['label'])) {
        $depth = $this->nestedSetExaminer->getDepth($entity);
        if ($depth > 0) {
          $indentation = [
            '#theme' => 'indentation',
            '#size' => $depth,
          ];
          $row['label']['#prefix'] = $this->renderer->render($indentation);
        }

        $row['label']['id'] = [
          '#type' => 'hidden',
          '#value' => $entity->id(),
          '#attributes' => [
            'class' => ['id'],
          ],
        ];
        $row['label']['parent'] = [
          '#type' => 'hidden',
          // Yes, default_value on a hidden. It needs to be changeable by the
          // javascript.
          '#default_value' => $this->nestedSetExaminer->getParentId($entity),
          '#attributes' => [
            'class' => ['parent'],
          ],
        ];
        $row['label']['depth'] = [
          '#type' => 'hidden',
          // Same as above, the depth is modified by javascript, so it's a
          // default_value.
          '#default_value' => $depth,
          '#attributes' => [
            'class' => ['depth'],
          ],
        ];
      }
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    // Only add the pager if a limit is specified.
    // @todo Using a pager works, but potentially leads to the case where
    //   ancestry of the first element is on the previous page. Find a better
    //   solution for this.
    if ($this->limit) {
      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Flatten the value structure.
    $hierarchy_values = [];
    foreach ($form_state->getValue($this->entitiesKey) as $id => $values) {
      $hierarchy_values[$id] = [];
      if (isset($values['label'])) {
        $hierarchy_values[$id]['parent'] = $values['label']['parent'];
        $hierarchy_values[$id]['depth'] = $values['label']['depth'];
      }
      else {
        $hierarchy_values[$id]['parent'] = '';
        $hierarchy_values[$id]['depth'] = 0;
      }
      $hierarchy_values[$id]['weight'] = $values['weight'];
    }

    // Sort the array by depth first and by weight second. This ensures that the
    // loop below will build up the nested set in the correct order. Note that
    // the resulting order may be unintuitive because children of different
    // parents are mingled among each other.
    uasort($hierarchy_values, function (array $a, array $b) {
      if ($a['depth'] != $b['depth']) {
        return ($a['depth'] > $b['depth']) ? 1 : -1;
      }
      if ($a['weight'] != $b['weight']) {
        return ($a['weight'] > $b['weight']) ? 1 : -1;
      }
      return 0;
    });

    // Build up the nested set structure recursively. The top-level elements are
    // the keys of the array, with arrays as values. These arrays in turn have
    // the respective children as keys with arrays as values. The child arrays
    // in turn have the respective grandchildren as keys and so forth.
    foreach ($hierarchy_values as $id => $element_values) {
      NestedArray::setValue($this->nestedSet, $this->getParents($hierarchy_values, $id), []);
    }

    // @todo Validate that the hierarchy is valid.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Do this in a batch.
    $start = $this->nestedSetExaminer->getMaxRight() + 1;
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    foreach ($this->updateEntities($start) as $entity) {
      $entity->save();
    }

    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    foreach ($this->updateEntities() as $entity) {
      $entity->save();
    }

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * Recursively gets all parents of the given ID in the hierarchy.
   *
   * @param array[] $hierarchy_values
   *   The complete set of (flat) hierarchy values.
   * @param int|string $id
   *   The ID of the element to fetch the parents for.
   *
   * @return int[]|string[]
   *   A list of parents, including the given ID itself as the last element.
   */
  protected function getParents($hierarchy_values, $id) {
    $parents = [];
    if ($parent = $hierarchy_values[$id]['parent']) {
      $parents = array_merge($parents, $this->getParents($hierarchy_values, $parent));
    }
    $parents[] = $id;
    return $parents;
  }

  /**
   * Updates the position values of all entities using the nested set.
   *
   * Sets the position values of the entities by recursively traversing the
   * nested set while passing the common index by reference.
   *
   * @param int $start
   *   (optional) The starting point of the position values. Defaults to 0.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The updated entities.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function updateEntities($start = 1) {
    // The index is always incremented in passing so the first incrementation
    // should yield the start value.
    $index = $start - 1;
    // Keep a list of visited entities so that we can save the entities in the
    // correct order.
    $entities = [];
    foreach ($this->nestedSet as $top_level_element => $children) {
      $this->setPositionValues($this->entities[$top_level_element], $children, $index, $entities);
    }
    return $entities;
  }

  /**
   * Sets the position values for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to set the position values for.
   * @param array $children
   *   The children of the given entity. This should be a partial nested set
   *   structure, including all of the descendants.
   * @param int $index
   *   The index of the nested set model. After being incremented by one this
   *   will be set to the left position value. It will then be incremented
   *   for any descendants and subsequently incremented again and set to the
   *   right position value.
   * @param array $updated_entities
   *   A list of entities to denote in which order the entities were visited
   *   during the traversal. Any descendants will be added to this list and
   *   finally the entity itself will be added, as well.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function setPositionValues(ContentEntityInterface $entity, array $children, &$index, array &$updated_entities) {
    $items = $entity->get($this->nestedSetFieldName);
    $items->lft = ++$index;

    foreach ($children as $child => $grandchildren) {
      $this->setPositionValues($this->entities[$child], $grandchildren, $index, $updated_entities);
    }

    $items->rgt = ++$index;
    $updated_entities[] = $entity;
  }

}
