<?php

namespace Drupal\nested_set\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\nested_set\NestedSet\NestedSetExaminerFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list builder for entities using a translatable nested set field.
 *
 * See NestedSetListBuilder for more information on the usage of this.
 *
 * @see \Drupal\nested_set\Entity\NestedSetListBuilder
 */
abstract class TranslatableNestedSetListBuilder extends NestedSetListBuilder {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a translatable nested set list builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\nested_set\NestedSet\NestedSetExaminerFactory $nested_set_factory
   *   The nested set factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, FormBuilderInterface $form_builder, NestedSetExaminerFactory $nested_set_factory, RendererInterface $renderer, LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;

    parent::__construct($entity_type, $storage, $form_builder, $renderer, $nested_set_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeNestedSetExaminer(NestedSetExaminerFactory $nested_set_examiner_factory) {
    $this->nestedSetExaminer = $nested_set_examiner_factory->get(
      $this->entityTypeId,
      $this->nestedSetFieldName,
      $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId()
    );
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return \Drupal\Core\Entity\EntityListBuilder|\Drupal\nested_set\Entity\NestedSetListBuilder
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('form_builder'),
      $container->get('nested_set.examiner_factory'),
      $container->get('renderer'),
      $container->get('language_manager')
    );
  }

}
