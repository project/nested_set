<?php

namespace Drupal\nested_set\Migrate;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrateDestinationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Keeps track of the hierarchy of imported entities during a migration.
 *
 * When importing hierarchical entities, the entities are initially imported
 * without any hierarchy information and then the hierarchy is applied after all
 * rows have been imported.
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Account constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager, EntityFieldManager $entity_field_manager) {
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MigrateEvents::PRE_IMPORT => 'initializeState',
      MigrateEvents::PRE_ROW_SAVE => 'recordRow',
      MigrateEvents::POST_IMPORT => 'applyHierarchy',
    ];
  }

  /**
   * Reacts to a migration being started.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The migrate import event.
   */
  public function initializeState(MigrateImportEvent $event) {
    $destination = $event->getMigration()->getDestinationPlugin();
    if (!$this->isNestedSetDestination($destination)) {
      return;
    }

    $entity_type_id = $this->getEntityTypeId($destination);
    foreach (array_keys($this->getFieldBundles($entity_type_id)) as $field_name) {
      $state_key = $this->getStateKey($entity_type_id, $field_name);
      $this->state->set($state_key, new \SplObjectStorage());
    }
  }

  /**
   * Reacts to migrate row being saved.
   *
   * @param \Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function recordRow(MigratePreRowSaveEvent $event) {
    /* @var \Drupal\migrate\Plugin\migrate\destination\EntityContentBase $destination */
    $destination = $event->getMigration()->getDestinationPlugin();
    $entity_type_id = $this->getEntityTypeId($destination);

    $row = $event->getRow();
    $bundle = $destination->getBundle($row);
    foreach ($this->getFieldBundles($entity_type_id) as $field_name => $bundles) {
      if (!in_array($bundle, $bundles, TRUE)) {
        continue;
      }

      $values = [];
      foreach (['parent', 'weight'] as $property) {
        if ($row->hasDestinationProperty("$field_name/$property")) {
          $values[$property] = $row->getDestinationProperty("$field_name/$property");
          $row->removeDestinationProperty("$field_name/$property");
        }
      }

      $state_key = $this->getStateKey($entity_type_id, $field_name);
      $hierarchy_values = $this->state->get($state_key);
      if (!($hierarchy_values instanceof \SplObjectStorage)) {
        $event->logMessage('Hierarchy value storage is corrupt.', 'error');
        continue;
      }

      $id = (object) $row->getSourceIdValues();
      $hierarchy_values->attach($id, $values);
      $this->state->set($state_key, $hierarchy_values);
    }
  }

  /**
   * Reacts to a migration import being completed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\migrate\MigrateException
   */
  public function applyHierarchy(MigrateImportEvent $event) {
    $migration = $event->getMigration();
    $destination = $migration->getDestinationPlugin();
    if (!$this->isNestedSetDestination($destination)) {
      return;
    }

    $entity_type_id = $this->getEntityTypeId($destination);
    $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple();

    // If no weight is explicitly provided as part of the migration, sort the
    // entities by label.
    uasort($entities, function (ContentEntityInterface $a, ContentEntityInterface $b) {
      // Entities may override the label method even if they do not have an
      // entity key, so we call the method unconditionally. However, the method
      // may return NULL, so we need to cast it to an (empty) string in that
      // case.
      return strnatcasecmp((string) $a->label(), (string) $b->label());
    });
    $default_weights = array_combine(array_keys($entities), range(0, count($entities) - 1));

    foreach (array_keys($this->getFieldBundles($entity_type_id)) as $field_name) {
      $state_key = $this->getStateKey($entity_type_id, $field_name);

      $source_hierarchy_values = $this->state->get($state_key, []);
      if (!($source_hierarchy_values instanceof \SplObjectStorage)) {
        $event->logMessage('Hierarchy value storage is corrupt.', 'error');
        continue;
      }

      $hierarchy_values = [];
      // Check whether weights were provided in this migration or not.
      $has_weights = FALSE;
      foreach ($source_hierarchy_values as $source_ids) {
        $source_id_values = (array) $source_ids;
        $destination_ids = $migration->getIdMap()->lookupDestinationIds($source_id_values);
        /* @see \Drupal\migrate\MigrateExecutable::import() */
        $destination_id_values = reset($destination_ids);
        if (!$destination_id_values) {
          $event->logMessage(sprintf('Missing destination ID values for row with source IDs: %s', implode (', ', $source_id_values)));
        }
        /* @see \Drupal\migrate\Plugin\migrate\destination\Entity::getEntity() */
        $id = reset($destination_id_values);
        if (!$id) {
          $event->logMessage(sprintf('Missing destination ID for row with source IDs: %s', implode (', ', $source_id_values)));
        }

        $hierarchy_values[$id] = $source_hierarchy_values->offsetGet($source_ids);
        if (!$has_weights && isset($hierarchy_values[$id]['weight'])) {
          $has_weights = TRUE;
        }
      }
      
      foreach (array_keys($hierarchy_values) as $id) {
        $this->setDepth($hierarchy_values, $id);
      }
      if (!$has_weights) {
        foreach ($hierarchy_values as $id => &$values) {
          $values['weight'] = $default_weights[$id];
        }
      }

      // Sort the array by depth first and by weight second. This ensures that the
      // loop below will build up the nested set in the correct order. Note that
      // the resulting order may be unintuitive because children of different
      // parents are mingled among each other.
      uasort($hierarchy_values, function (array $a, array $b) {
        if ($a['depth'] != $b['depth']) {
          return ($a['depth'] > $b['depth']) ? 1 : -1;
        }
        if ($a['weight'] != $b['weight']) {
          return ($a['weight'] > $b['weight']) ? 1 : -1;
        }
        return 0;
      });

      $nested_set = [];
      // Build up the nested set structure recursively. The top-level elements are
      // the keys of the array, with arrays as values. These arrays in turn have
      // the respective children as keys with arrays as values. The child arrays
      // in turn have the respective grandchildren as keys and so forth.
      foreach ($hierarchy_values as $id => $element_values) {
        NestedArray::setValue($nested_set, $this->getParents($hierarchy_values, $id), []);
      }

      $index = 0;
      /** @var \Drupal\Core\Entity\ContentEntityInterface[] $updated_entities */
      $updated_entities = [];
      foreach ($nested_set as $top_level_element => $children) {
        $this->setPositionValues($entities, $field_name, $top_level_element, $children, $index, $updated_entities);
      }
      foreach ($updated_entities as $entity) {
        $entity->save();
      }

      $this->state->delete($state_key);
    }
  }

  /**
   * Checks whether the destination imports entities with a nested set field.
   *
   * @param \Drupal\migrate\Plugin\MigrateDestinationInterface $destination
   *   The migrate destination.
   *
   * @return bool
   *   TRUE if the destination has a nested set field; FALSE otherwise.
   */
  protected function isNestedSetDestination(MigrateDestinationInterface $destination) {
    if (!is_a($destination, EntityContentBase::class)) {
      return FALSE;
    }

    /* @see \Drupal\migrate\Plugin\migrate\destination\Entity::getEntityTypeId() */
    $entity_type_id = $this->getEntityTypeId($destination);
    if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
      return FALSE;
    }

    $field_map = $this->entityFieldManager->getFieldMapByFieldType('nested_set');
    if (!isset($field_map[$entity_type_id])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Sets the depth in the hierarchy values for an entity.
   *
   * @param array $hierarchy_values
   *   The complete set of hierarchy values.
   * @param int|string $id
   *   The ID of the entity to set the depth for.
   */
  protected function setDepth(array &$hierarchy_values, $id) {
    $values = &$hierarchy_values[$id];
    if (isset($values['depth'])) {
      return;
    }

    $parent = $values['parent'];
    if ($parent === NULL) {
      $values['depth'] = 0;
    }
    else {
      if (!isset($hierarchy_values[$parent]['depth'])) {
        $this->setDepth($hierarchy_values, $parent);
      }
      $values['depth'] = $hierarchy_values[$parent]['depth'] + 1;
    }
  }

  /**
   * Recursively gets all parents of the given ID in the hierarchy.
   *
   * @param array[] $hierarchy_values
   *   The complete set of (flat) hierarchy values.
   * @param int|string $id
   *   The ID of the element to fetch the parents for.
   *
   * @return int[]|string[]
   *   A list of parents, including the given ID itself as the last element.
   */
  protected function getParents($hierarchy_values, $id) {
    $parent_ids = [];
    /** @var \Drupal\ledger\Entity\Account $parent */
    $parent = $hierarchy_values[$id]['parent'];
    if ($parent !== NULL) {
      $parent_ids = array_merge($parent_ids, $this->getParents($hierarchy_values, $parent));
    }
    $parent_ids[] = $id;
    return $parent_ids;
  }

  /**
   * Sets the position values for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The complete set of entities.
   * @param string $field_name
   *   The field name of the nested set field.
   * @param int|string $entity_id
   *   The ID of the entity to set the position values for.
   * @param array $children
   *   The children of the given entity. This should be a partial nested set
   *   structure, including all of the descendants.
   * @param int $index
   *   The index of the nested set model. After being incremented by one this
   *   will be set to the left position value. It will then be incremented
   *   for any descendants and subsequently incremented again and set to the
   *   right position value.
   * @param array $updated_entities
   *   A list of entities to denote in which order the entities were visited
   *   during the traversal. Any descendants will be added to this list and
   *   finally the entity itself will be added, as well.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function setPositionValues(array $entities, $field_name, $entity_id, array $children, &$index, array &$updated_entities) {
    $items = $entities[$entity_id]->get($field_name);
    $items->lft = ++$index;

    foreach ($children as $child => $grandchildren) {
      $this->setPositionValues($entities, $field_name, $child, $grandchildren, $index, $updated_entities);
    }

    $items->rgt = ++$index;
    $updated_entities[] = $entities[$entity_id];
  }

  /**
   * Gets the entity type ID for the migration destination.
   *
   * @param \Drupal\migrate\Plugin\MigrateDestinationInterface $destination
   *
   * @return string|false
   *   The entity type ID or FALSE if no entity type ID could be determined.
   *
   * @see \Drupal\migrate\Plugin\migrate\destination\Entity::getEntityTypeId()
   */
  protected function getEntityTypeId(MigrateDestinationInterface $destination) {
    return substr($destination->getPluginId(), 7);
  }

  /**
   * Gets the list of bundles with nested set fields for the given entity type.
   *
   * @param string $entity_type_id
   *   The ID of the entity type.
   *
   * @return string[][]
   *   The list of bundles keyed by field name.
   */
  protected function getFieldBundles($entity_type_id) {
    $field_map = $this->entityFieldManager->getFieldMapByFieldType('nested_set');
    assert(isset($field_map[$entity_type_id]));
    $field_info = $field_map[$entity_type_id];
    return array_combine(array_keys($field_info), array_column($field_info, 'bundles'));
  }

  /**
   * The state key to use for the given entity type and field.
   *
   * @param string $entity_type_id
   *   The ID of the entity type.
   * @param string $field_name
   *   The name of the field.
   *
   * @return string
   *   The state key.
   */
  protected function getStateKey($entity_type_id, $field_name) {
    return "nested_set.migrate_import.$entity_type_id.$field_name";
  }

}
