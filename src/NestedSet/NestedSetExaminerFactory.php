<?php

namespace Drupal\nested_set\NestedSet;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Creates a nested set examiner for a given entity type, field and language.
 */
class NestedSetExaminerFactory {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Creates a nested set for the given entity type, field and language.
   *
   * @param string $entity_type_id
   *   The ID of the entity type.
   * @param string $field_name
   *   The name of the field.
   * @param string $langcode
   *   (optional) The language code.
   *
   * @return \Drupal\nested_set\NestedSet\NestedSetExaminer
   *   The nested set examiner.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($entity_type_id, $field_name, $langcode = NULL) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    return new NestedSetExaminer($storage, $field_name, $langcode);
  }

}
