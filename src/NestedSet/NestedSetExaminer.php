<?php

namespace Drupal\nested_set\NestedSet;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Allows examining a nested set model that is persisted in the database.
 *
 * The nested field stores its values in a way that all information about the
 * hierarchy can be retrieved using entity queries. This class solely exists to
 * make interacting with the nested set data more convenient, and to avoid
 * having to duplicate common queries.
 *
 * Each nested set encompasses all field values for a given field on a given
 * entity type. In particular, if the field is not required the nested set is
 * comprised of only those entities where the field is populated. A nested set
 * field is always a single-value field.
 *
 * If the field is translatable each translation language will have a distinct
 * nested set model.
 *
 * Note that this implementation of the nested set model allows for free
 * intervals. An element with the positions 1 and 5, for example, that has a
 * single child with the positions 2 and 3 is considered a valid nested set,
 * even though there is no element at position 4.
 *
 * @see https://en.wikipedia.org/wiki/Nested_set_model
 */
class NestedSetExaminer {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The language code.
   *
   * @var string
   */
  protected $langcode;

  /**
   * Creates a nested set.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param string $field_name
   *   The field name.
   * @param string $langcode
   *   (optional) The language code.
   */
  public function __construct(EntityStorageInterface $storage, $field_name, $langcode = NULL) {
    $this->storage = $storage;
    $this->fieldName = $field_name;
    $this->langcode = $langcode;
  }

  /**
   * Returns the maximum right position of any element in the nested set.
   *
   * @return int
   *   The maximum right position. 0 is returned if the nested set is empty.
   */
  public function getMaxRight() {
    $alias = 'max';
    $result = $this->storage->getAggregateQuery()
      ->aggregate($this->fieldName . '.rgt', 'MAX', $this->langcode, $alias)
      ->accessCheck(FALSE)
      ->execute();
    if (!$result) {
      return 0;
    }

    $row = reset($result);
    return (int) $row[$alias];
  }

  /**
   * Returns the parent of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The parent entity or NULL, if the entity is a top-level element.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getParentId(ContentEntityInterface $entity) {
    // Of the ancestors, the parent is the one with the left position that is
    // furthest right.
    $result = $this->getAncestorQuery($entity)
      ->sort($this->fieldName . '.lft', 'DESC', $this->langcode)
      ->range(0, 1)
      ->execute();

    if ($result) {
      return reset($result);
    }
  }

  /**
   * Returns the depth of the entity in the nested set.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return int
   *   The entities depth in the nested set. 0 is returned if the entity is a
   *   top-level element.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getDepth(ContentEntityInterface $entity) {
    // The depth is equal to the amount of ancestors.
    return $this->getAncestorQuery($entity)
      ->count()
      ->execute();
  }

  /**
   * Gets the weight of the entity among its siblings.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return int
   *   The entity's weight among its sibling. The first of the siblings has the
   *   weight 0, the second the weight 1, and so forth.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getWeight(ContentEntityInterface $entity) {
    // Calculating the weight is something that cannot be done with a nested set
    // without joining the same table together which an entity query is not
    // capable of. Thus, we compute the weight manually by iteratively finding
    // all previous siblings.
    $own_left = $this->getLeftPosition($entity);

    $left_margin = 0;
    if ($parent_id = $this->getParentId($entity)) {
      $parent = $this->storage->load($parent_id);
      $left_margin = $this->getLeftPosition($parent);
    }

    $left_alias = 'left';
    $right_alias = 'right';

    $result = $this->storage->getAggregateQuery()
      ->aggregate($this->fieldName . '.lft', '', $this->langcode, $left_alias)
      ->aggregate($this->fieldName . '.rgt', '', $this->langcode, $right_alias)
      ->sort($this->fieldName . '.lft', 'ASC')
      ->condition($this->fieldName . '.lft', $left_margin, '>', $this->langcode)
      ->condition($this->fieldName . '.rgt', $own_left, '<', $this->langcode)
      ->accessCheck(FALSE)
      ->execute();

    $left_positions = array_map('intval', array_column($result, $left_alias));
    $right_positions = array_map('intval', array_column($result, $right_alias));

    $weight = 0;
    while ($left_positions) {
      $left_position = array_shift($left_positions);
      $right_position = array_shift($right_positions);

      $children_count = ($right_position - $left_position - 1) / 2;
      $left_positions = array_slice($left_positions, $children_count);
      $right_positions = array_slice($right_positions, $children_count);

      ++$weight;
    }

    return $weight;
  }

  /**
   * Find a free interval within the entity's interval.
   *
   * A free interval is an interval that is not occupied by any child element in
   * the nested set. Temporarily having free intervals is necessary when
   * updating nested sets. For example, when inserting a child into an interval,
   * the interval must first be expanded to include a free interval for the
   * child to fit in. That way the nested set stays valid the whole time.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to find the free interval for.
   *
   * @return int[]|bool
   *   The interval as a list of left and right position, if any was found;
   *   FALSE otherwise.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFreeInterval(ContentEntityInterface $entity) {
    $parent_left = $this->getLeftPosition($entity);
    $parent_right = $this->getRightPosition($entity);

    if (($parent_right - $parent_left) < 3) {
      return FALSE;
    }

    $left_alias = 'left';
    $right_alias = 'right';

    $result = $this->storage->getAggregateQuery()
      ->aggregate($this->fieldName . '.lft', '', $this->langcode, $left_alias)
      ->aggregate($this->fieldName . '.rgt', '', $this->langcode, $right_alias)
      ->condition($this->fieldName . '.lft', $parent_left, '>', $this->langcode)
      ->condition($this->fieldName . '.rgt', $parent_right, '<', $this->langcode)
      ->sort($this->fieldName . '.lft', 'ASC', $this->langcode)
      ->accessCheck(FALSE)
      ->execute();

    if (!$result) {
      return [$parent_left + 1, $parent_right - 1];
    }

    $to_int = function ($string) { return (int) $string; };
    $left_positions = array_map($to_int, array_column($result, $left_alias));
    $right_positions = array_map($to_int, array_column($result, $right_alias));

    $nested_set = array_fill($parent_left + 1, $parent_right - $parent_left - 1, TRUE);
    foreach ($left_positions as $index => $left) {
      $right = $right_positions[$index];

      for ($position = $left; $position <= $right; ++$position) {
        $nested_set[$position] = FALSE;
      }
    }

    $width = -1;
    foreach ($nested_set as $position => $value) {
      if ($value) {
        ++$width;
      }
      else {
        if ($width >= 1) {
          return [$position - 1 - $width, $position - 1];
        }
        $width = -1;
      }
    }

    if ($value >= 1) {
      return [$position - $width, $position];
    }

    return FALSE;
  }

  /**
   * Checks whether the current nested set is valid.
   *
   * @param int $new_left
   * @param int $new_right
   * @param int|string $id
   *
   * @return bool
   *   TRUE if the nested set is valid; FALSE otherwise.
   */
  public function isValidNewPosition($new_left, $new_right, $id = NULL) {
    $left_alias = 'left';
    $right_alias = 'right';
    $query = $this->storage->getAggregateQuery()
      ->aggregate($this->fieldName . '.lft', '', $this->langcode, $left_alias)
      ->aggregate($this->fieldName . '.rgt', '', $this->langcode, $right_alias)
      ->sort($this->fieldName . '.lft', 'ASC')
      ->exists($this->fieldName . '.lft', $this->langcode)
      ->exists($this->fieldName . '.rgt', $this->langcode)
      ->accessCheck(FALSE);
    if ($id !== NULL) {
      $field_name = $this->storage->getEntityType()->getKey('id');
      $query->condition($field_name, $id, '<>');
    }
    $result = $query->execute();

    if (!$result) {
      return TRUE;
    }

    $left_positions = array_map('intval', array_column($result, $left_alias));
    $right_positions = array_map('intval', array_column($result, $right_alias));

    $left_positions[] = $new_left;
    $right_positions[] = $new_right;
    array_multisort($left_positions, $right_positions);

    // First make sure that all values are unique.
    if ($left_positions !== array_unique($left_positions)) {
      return FALSE;
    }
    if ($right_positions !== array_unique($right_positions)) {
      return FALSE;
    }

    $min = min($left_positions);
    $nested_set = array_fill($min, max($right_positions) - $min + 1, FALSE);
    foreach ($left_positions as $index => $left) {
      $right = $right_positions[$index];

      // The element is only valid if the left position is left of the right
      // position.
      if ($left >= $right) {
        return FALSE;
      }

      // Because the left positions are sorted in ascending order, children will
      // always be added after their parents. Thus, the entire interval consumed
      // by the element should be free.
      for ($position = $left; $position <= $right; ++$position) {
        if ($nested_set[$position]) {
          return FALSE;
        }
      }

      $nested_set[$left] = TRUE;
      $nested_set[$right] = TRUE;
    }

    return TRUE;
  }

  /**
   * Gets a query to select the ancestors of an entity.
   *
   * The ancestors of the element are all elements whose interval completely
   * encompasses this element's interval.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The given entity.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The ancestor query.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getAncestorQuery(ContentEntityInterface $entity) {
    return $this->storage->getQuery()
      ->condition($this->fieldName . '.lft', $this->getLeftPosition($entity), '<', $this->langcode)
      ->condition($this->fieldName . '.rgt', $this->getRightPosition($entity), '>', $this->langcode)
      ->accessCheck(FALSE);
  }

  /**
   * Gets a query to select elements contained within an interval.
   *
   * @param int $left
   *   The left margin that all elements need to be to the right of.
   * @param int $right
   *   The right margin that all elements need to be to the left of.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The interval query.
   */
  protected function getIntervalQuery($left, $right) {
    return $this->storage->getQuery()
      ->condition($this->fieldName . '.lft', $left, '>', $this->langcode)
      ->condition($this->fieldName . '.rgt', $right, '<', $this->langcode)
      ->accessCheck(FALSE);
  }

  /**
   * Gets the left position of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return int
   *   The entity's left position.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getLeftPosition(ContentEntityInterface $entity) {
    return (int) $entity->get($this->fieldName)->lft;
  }

  /**
   * Gets the right position of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return int
   *   The entity's right position.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getRightPosition(ContentEntityInterface $entity) {
    return (int) $entity->get($this->fieldName)->rgt;
  }

}
