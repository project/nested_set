<?php

namespace Drupal\nested_set\Plugin\Field\FieldType;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;

/**
 * Provides a field type for managing entities as nested sets.
 *
 * @see \Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem
 * @see https://en.wikipedia.org/wiki/Nested_set_model
 *
 * @FieldType(
 *   id = "nested_set",
 *   label = @Translation("Hierarchy"),
 *   description = @Translation("This field stores an entity's position in a hierarchy."),
 *   cardinality = 1,
 *   constraints = {
 *     "ValidNestedSet" = {},
 *   },
 * )
 *
 * @see \Drupal\nested_set\NestedSet\NestedSetExaminer
 */
class NestedSetItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      // Valid size property values include: 'tiny', 'small', 'medium', 'normal'
      // and 'big'.
      'size' => 'normal',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Use abbreviated column names, because 'left' and 'right' are reserved
    // words in MySQL.
    /* @see http://dev.mysql.com/doc/mysql/en/reserved-words.html */
    /* @see https://www.drupal.org/project/drupal/issues/2986452 */
    $properties['lft'] = DataDefinition::create('integer')
      ->setLabel(t('Left position'));
      // @todo Make this required. It currently cannot be required, because if
      //   it were, setting the 'prt' property but not the 'lft' and 'rgt'
      //   properties will make the entity fail validation even though it may
      //   actually be in a valid state.
    $properties['rgt'] = DataDefinition::create('integer')
      ->setLabel(t('Right position'));
      // @todo Make this required, see above.

    $entity_type_id = $field_definition->getTargetEntityTypeId();
    // The 'parent' property is already used for the typed-data parent of the
    // field item (in this case, the respective field item list) so we cannot
    // use that as the property name and using an abbreviation is consistent
    // with the other properties.
    // @todo Actually compute this value from the 'lft' and 'rgt' properties if
    //   requested.
    $properties['prt'] = DataReferenceDefinition::create('entity')
      ->setLabel('Parent')
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setTargetDefinition(EntityDataDefinition::create($entity_type_id))
      ->addConstraint('EntityType', $entity_type_id);

    // @todo Add computed properties for the children, depth, weight and width.

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    // The left and right position are equally important, but the left position
    // is often useful for sorting, and the semantic of the main property name
    // is vague anyway.
    return 'lft';
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'lft' => [
          'type' => 'int',
          'unsigned' => TRUE,
          // Expose the 'size' setting in the field item schema. For instance,
          // supply 'big' as a value to produce a 'bigint' type.
          'size' => $field_definition->getSetting('size'),
        ],
        'rgt' => [
          'type' => 'int',
          'unsigned' => TRUE,
          // See above.
          'size' => $field_definition->getSetting('size'),
        ],
      ],
      // @todo Add unique key constraints
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (($this->lft !== NULL) && ($this->rgt !== NULL)) {
      return FALSE;
    }
    // Setting a parent must make a field item non-empty, otherwise it would be
    // removed by FieldItemList::filterEmptyItems() before the parent can used
    // to determine the position in NestedSetItem::preSave().
    if ($this->prt && ($this->prt instanceof ContentEntityInterface)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function applyDefaultValue($notify = TRUE) {
    if ($this->getFieldDefinition()->isRequired()) {
      $this->lft = $this->getNestedSetExaminer()->getMaxRight() + 1;
      $this->rgt = $this->lft + 1;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preSave() {
    if ((($this->lft === NULL) || ($this->rgt === NULL)) && ($this->prt && ($this->prt instanceof ContentEntityInterface))) {
      // If the element is to placed within a parent interval, that interval
      // needs to have sufficient free space.
      $interval = $this->getNestedSetExaminer()->getFreeInterval($this->prt);
      if ($interval) {
        list($this->lft) = $interval;
        $this->rgt = $this->lft + 1;
      }
      else {
        $entity_type_id = $this->getEntity()->getEntityTypeId();
        $field_name = $this->getFieldDefinition()->getName();
        throw new EntityMalformedException(sprintf("The nested set model for entity type '%s' and field '%s' does not have a free interval within %d and %d", $entity_type_id, $field_name, $this->prt->get($field_name)->lft, $this->prt->get($field_name)->rgt));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // @todo This is not always a valid value.
    $left = mt_rand(1, 999);
    return [
      'lft' => $left,
      'rgt' => $left + 1,
    ];
  }

  /**
   * Gets the nested set examiner for this field.
   *
   * @return \Drupal\nested_set\NestedSet\NestedSetExaminer
   *   The nested set examiner.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNestedSetExaminer() {
    $field_definition = $this->getFieldDefinition();
    /* @var \Drupal\nested_set\NestedSet\NestedSetExaminerFactory $nested_set_examiner_factory */
    $nested_set_examiner_factory = \Drupal::service('nested_set.examiner_factory');

    $entity_type_id = $this->getEntity()->getEntityTypeId();
    $field_name = $field_definition->getName();
    $langcode = $field_definition->isTranslatable() ? $this->getLangcode() : NULL;
    return $nested_set_examiner_factory->get($entity_type_id, $field_name, $langcode);
  }

}
