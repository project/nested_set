<?php

namespace Drupal\nested_set\Plugin\Validation\Constraint;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\nested_set\Plugin\Field\FieldType\NestedSetItem;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidNestedSetConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function validate($value, Constraint $constraint) {
    if (!($constraint instanceof ValidNestedSetConstraint)) {
      throw new UnexpectedTypeException($constraint, ValidNestedSetConstraint::class);
    }
    if (!($value instanceof NestedSetItem)) {
      $this->context->addViolation($constraint->invalidValueMessage);
      return;
    }

    $field_definition = $value->getFieldDefinition();
    if ($field_definition->isRequired() || !$value->isEmpty()) {
      $nested_set_examiner = $value->getNestedSetExaminer();
      if ((($value->lft === NULL) || ($value->rgt === NULL)) && ($value->prt && ($value->prt instanceof ContentEntityInterface))) {
        // If the element is to placed within a parent interval, that interval
        // needs to have sufficient free space.
        $interval = $nested_set_examiner->getFreeInterval($value->prt);
        if ($interval) {
          list($value->lft) = $interval;
          $value->rgt = $value->lft + 1;
        }
        else {
          $this->context->addViolation($constraint->invalidNewPositionMessage, [
            '%field' => $field_definition->getLabel(),
            '%left' => $nested_set_examiner->getLeftPosition($value->prt),
            '%right' => $nested_set_examiner->getRightPosition($value->prt),
          ]);
          return;
        }
      }
      $entity = $value->getEntity();
      $exclude = [];
      if (!$entity->isNew()) {
        $exclude[$entity->getEntityType()->getKey('id')] = $entity->id();
      }
      if (!$nested_set_examiner->isValidNewPosition($value->lft, $value->rgt, $entity->isNew() ? NULL : $entity->id())) {
        if ($field_definition->isTranslatable()) {
          $language_manager = \Drupal::languageManager();
          $this->context->addViolation($constraint->invalidTranslatableNestedSetMessage, [
            '%field' => $field_definition->getLabel(),
            '%language' => $language_manager->getLanguage($value->getLangcode())->getName(),
          ]);
        }
        else {
          $this->context->addViolation($constraint->invalidNestedSetMessage, [
            '%field' => $field_definition->getLabel(),
          ]);
        }
      }
    }
  }

}
