<?php

namespace Drupal\nested_set\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint to make sure a nested set is valid.
 *
 * @Constraint(
 *   id = "ValidNestedSet",
 *   label = @Translation("Valid hierarchy"),
 * )
 */
class ValidNestedSetConstraint extends Constraint {

  public $invalidValueMessage = 'The value to check must be a nested set field item.';

  public $invalidNewPositionMessage = 'The nested set model for %field does not have a free interval within %left and %right';

  public $invalidNestedSetMessage = 'The %field hierarchy is invalid.';

  public $invalidTranslatableNestedSetMessage = 'The %field hierarchy in %language is invalid.';

}
